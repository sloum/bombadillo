/*
* Copyright (C) 2022 Brian Evans
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package finger

import (
	"fmt"
	"io/ioutil"
	"net"
	"time"
)

func Finger(host, port, resource string) (string, error) {
	addr := fmt.Sprintf("%s:%s", host, port)

	timeOut := time.Duration(3) * time.Second
	conn, err := net.DialTimeout("tcp", addr, timeOut)
	if err != nil {
		return "", err
	}

	defer conn.Close()

	_, err = conn.Write([]byte(resource + "\r\n"))
	if err != nil {
		return "", err
	}

	result, err := ioutil.ReadAll(conn)
	if err != nil {
		return "", err
	}
	return string(result), nil
}
