/*
* Copyright (C) 2022 Brian Evans
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

// ERRS maps commands to their syntax error message
var ERRS = map[string]string{
	"A":         "`a [target] [name...]`",
	"ADD":       "`add [target] [name...]`",
	"D":         "`d [bookmark-id]`",
	"DELETE":    "`delete [bookmark-id]`",
	"B":         "`b [[bookmark-id]]`",
	"BOOKMARKS": "`bookmarks [[bookmark-id]]`",
	"C":         "`c [link_id]` or `c [setting]`",
	"CHECK":     "`check [link_id]` or `check [setting]`",
	"H":         "`h`",
	"HOME":      "`home`",
	"J":         "`j [[history_position]]`",
	"JUMP":      "`jump [[history_position]]`",
	"P":         "`p [host]`",
	"PURGE":     "`purge [host]`",
	"Q":         "`q`",
	"QUIT":      "`quit`",
	"R":         "`r`",
	"RELOAD":    "`reload`",
	"SEARCH":    "`search [[keyword(s)...]]`",
	"S":         "`s [setting] [value]`",
	"SET":       "`set [setting] [value]`",
	"W":         "`w [target]`",
	"WRITE":     "`write [target]`",
	"VERSION":   "`version`",
	"?":         "`? [[command]]`",
	"HELP":      "`help [[command]]`",
}
